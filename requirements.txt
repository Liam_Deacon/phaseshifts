# This is an implicit value, here for clarity
--index-url https://pypi.python.org/simple/

# install-requires
numpy >= 1.11.1
scipy >= 0.18
ase >= 3.11
periodictable

# extra-requires
pycifrw
#spglib
#pymatgen
#qtsix
#PyQt4
#pygtk  # for ase GUI only => not really needed
