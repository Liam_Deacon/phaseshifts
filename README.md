[![wercker status](https://app.wercker.com/status/8133085cc3734ba0441339368093e484/s/ "wercker status")](https://app.wercker.com/project/byKey/8133085cc3734ba0441339368093e484)
[![Build status](https://ci.appveyor.com/api/projects/status/r32540sd0va20qqf?svg=true)](https://ci.appveyor.com/project/Liam_Deacon/phaseshifts)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/29aea857bb044eafbf5286e50e3a47e5)](https://www.codacy.com/app/Liam_Deacon/phaseshifts?utm_source=Liam_Deacon@bitbucket.org&amp;utm_medium=referral&amp;utm_content=Liam_Deacon/phaseshifts&amp;utm_campaign=Badge_Grade)
[![Language](https://img.shields.io/badge/language-python_2.7/3.5-ff69b4.svg "Python programming language")](https://wiki.python.org/moin/Python2orPython3)
[![License](http://img.shields.io/badge/license-gpl3-blue.svg "GNU Public License v3.0")](http://www.gnu.org/licenses/gpl-3.0.html)
[![Implementation](http://img.shields.io/badge/implementation-cpython-blue.svg "Requires CPython")](https://www.python.org/downloads/)
[![Issues](https://img.shields.io/badge/issues-open-yellow.svg "Issues")](https://bitbucket.org/api/1.0/repositories/Liam_Deacon/phaseshifts/issues/?limit=0&status=new&status=open)
[![Coverage Status](https://coveralls.io/repos/bitbucket/Liam_Deacon/phaseshifts/badge.svg?branch=master)](https://coveralls.io/bitbucket/Liam_Deacon/phaseshifts?branch=master)
[![Dependency Status](https://gemnasium.com/badges/bitbucket.org/Liam_Deacon/phaseshifts.svg)](https://gemnasium.com/bitbucket.org/Liam_Deacon/phaseshifts)
[![Requirements Status](https://requires.io/bitbucket/Liam_Deacon/phaseshifts/requirements.svg?branch=develop)](https://requires.io/bitbucket/Liam_Deacon/phaseshifts/requirements/?branch=develop)
[![PyPI version](https://badge.fury.io/py/phaseshifts.svg)](https://badge.fury.io/py/phaseshifts)

# PHASESHIFTS #

## Getting Started ##

This is the developer's readme - for more information please visit the project [wiki](https://bitbucket.org/Liam_Deacon/phaseshifts/wiki/Home)

For a general overview of the project or for the latest production release, please look [here](https://pypi.python.org/pypi)

A list of current issues and enhancement proposals can be found at: https://bitbucket.org/Liam_Deacon/phaseshifts/issues?status=new&status=open

### Who do I talk to? ###

* Liam Deacon <liam.m.deacon@gmail.com> for general information and help with this project.
* John Rundgren <jru@kth.se> for information about the EEASiSSS backend.
* Michel Van Hove <vanhove@hkbu.edu.hk> for information and queries regarding the Barbieri/Van Hove backend.
* Eric Shirley <eric.shirley@nist.gov> for information about the hartfock routines (used for calculating atomic charge densities).