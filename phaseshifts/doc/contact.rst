.. _contact:

*******
Contact
*******

This package is developed/maintained in my spare time so any bug reports, patches, 
or other feedback are very welcome and should be sent to: liam.m.deacon@gmail.com

The project is in the early developmental stages and so anyone who wishes to get 
involved are most welcome (simply contact me using the email above).

Queries relating to the Barbieri/Van Hove and EEASiSSS backends should be 
directed to Michel Van Hove <vanhove@hkbu.edu.hk> or John Rundgren <jru@kth.se>, 
respectively. Whereas questions pertaining to the atomic charge density 
calculations (using muffin-tin spheres) should be directed to 
Eric Shirley <eric.shirley@nist.gov>. 
