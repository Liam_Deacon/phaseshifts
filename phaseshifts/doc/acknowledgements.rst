.. _acknowledgements:

****************
Acknowledgements
****************

As with all scientific progress, we stand on the shoulders of giants. If this 
package is of use to you in publishing papers then please acknowledge the 
following people who have made this package a reality:

 - **A. Barbieri** and **M.A. Van Hove** - who developed most of the original 
   Fortran code. Use *A. Barbieri and M.A. Van Hove, private communication.*
 
 - **E.L. Shirley** - who developed part of the fortran code during work 
   towards his PhD thesis (refer to the thesis: 
   *E.L. Shirley, "Quasiparticle calculations in 
   atoms and many-body core-valence partitioning", 
   University of Illinois, Urbana, 1991*).

 - **J. Rundgren** - who developed the EEASiSSS package and collaborated on 
   numerous improvements to the underlying Fortran code base. Please cite
   *J. Rundgren, Phys. Rev. B 68 125405 (2003).*

 - **Christoph Gohlke** - who developed the elements.py module used 
   extensively throughout for the modelling convenience functions 
   (see 'elements.py' for license details). 

I would also be grateful if you acknowledge this python package 
(*phaseshifts*) as: *L.M. Deacon, private communication.*


Thanks
======

I wish to personally add a heartfelt thanks to both Eric Shirley and 
Michel Van Hove who have kindly allowed the use of their code in the 
``libphsh.f`` file needed for the underlying low-level functions in this 
package. 

In addition, I am indebted to the help and guidance of John Rundgren, who has 
closely collaborated on EEASiSSS support and whose stimulating conversations 
have guided much of the newer innovations for the Python phaseshifts package.
