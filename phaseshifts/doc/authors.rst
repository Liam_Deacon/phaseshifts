.. _authors:

***********
Author list
***********

Below is a list of contributors who have helped to develop this package:
 * Liam Deacon - *current maintainer*
 * Eric Shirley - *atomic charge density calculations*
 * Michel Van Hove - *Barbieri/Van Hove phase shift package*
 * John Rundgren - *EEASiSSS phase shift package*

Get Involved
============

If you would like to get involved in the phaseshifts project then
please email liam.m.deacon@gmail.com.